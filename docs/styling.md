# scss & styling

## extrnal tools & techniques

We are using ['inuit.css'](https://github.com/inuitcss/inuitcss) library. which is very advanced but lacks documentions.
inuit is based on ['OOCSS'](https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/) a method to create meaningful class names.
o- stands for Object: a handy tool to be used throught the app. a change in an object can have applciation wide effects. use wisely
c- component: a more specific class used for subjective tasks
u- utility: a modifier to use common patterns & techniques

## Files
in the "styles" folder there are 3 files

### globals.scss
*caution*: _DO NOT_ place regular css definitions or anything that is compiled to css code
as this file is imported in MANY places and this will make horrible code duplications.
use only for scss scripting propuses such as variables and mixins etc.

### brainerz.scss
Our global scss file. write global scss here.

### styles.scss
this is what included in the angular compilation process. the root of all the styles.
used mainly to import both inuit and my global styles.
