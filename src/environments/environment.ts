// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  serverUrl: 'http://localhost:5000/brainerrrz/us-central1/',
  firebase: {
    apiKey: 'AIzaSyC-Vv4JTEsOzrpWfR2Xc4STDzQJ46HVuT4',
    authDomain: '<your-project-authdomain>',
    databaseURL: 'https://brainerrrz.firebaseio.com',
    projectId: 'brainerrrz',
    storageBucket: '<your-storage-bucket>',
    messagingSenderId: '<your-messaging-sender-id>'
  }
};
