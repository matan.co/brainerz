import { Component, OnInit, Input } from '@angular/core';
import { PlayTable } from '../../models/play-table';
import { printCountDown } from '../../utils/utils';
import { images, createImageConsts } from '../../utils/type-safe-image-paths';
import { CoreService } from '../../core.service';

@Component({
  selector: 'brz-play-table',
  templateUrl: './play-table.component.html',
  styleUrls: ['./play-table.component.scss']
})
export class PlayTableComponent implements OnInit {
  @Input() table: PlayTable;
  msLeftToJoin: number;
  countDownString: string;
  timerObj
  images = images;

  assets;

  constructor() { }

  ngOnInit() {
    this.initTimer();
    this.assets = createImageConsts(imgConsts => ({
      symbol: imgConsts.subjects.symbols + this.table.subject.name + ".svg",
      miniSymbol: imgConsts.subjects.miniSymbols + this.table.subject.name + ".svg"
    }));
    console.log("initing table ", this.assets.miniSymbol);
  }

  private initTimer(){
    this.countDownString = "00:00:00";
    this.msLeftToJoin =
           this.table.closeTime.valueOf() - new Date().valueOf();

        this.timerObj = setInterval(()=>{
          this.countDownString = printCountDown(new Date(this.msLeftToJoin-= 1000));
        }, 1000, 0)
  }


  ngOnDestroy(){
    clearInterval(this.timerObj);
  }
}
