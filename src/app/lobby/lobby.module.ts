import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LobbyComponent } from './lobby.component';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { PlayTableComponent } from './play-table/play-table.component';
import { WalletComponent } from './wallet/wallet.component';
import { MiscModule } from '../misc/misc.module';
import { SignupComponent } from './signup/signup.component';

@NgModule({
  imports: [
    CommonModule,
    MiscModule,
    RouterModule.forChild([
      { path: '', component: LobbyComponent,
          children: [ {path: 'signup', component: SignupComponent}] },
      { path: 'wallet', component: WalletComponent}])
  ],
  declarations: [SignupComponent, LobbyComponent, NavbarComponent, PlayTableComponent, WalletComponent],
  providers: []
})
export class LobbyModule { }
