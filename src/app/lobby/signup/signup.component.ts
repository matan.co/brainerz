import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { PlayerDTO, createPlayerAction } from "@brainerz/common";
import { FirebaseService } from "../../firebase.service";
import { CoreService } from "../../core.service";
import { Router } from "@angular/router";

@Component({
  selector: 'brz-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  player: PlayerDTO = {nickname: "", avatar: "1", pointsBalance: 1000};

  constructor(private firebase: FirebaseService, private coreService: CoreService, private router: Router) {

   }

  ngOnInit() {
  }

  chooseImage(e: MouseEvent){
    console.log(e);
    this.player.avatar = (<HTMLElement>e.toElement).dataset.avatar;
  }

  register(){
    console.log("registrating");
    this.firebase.request({ data: this.player,  action: createPlayerAction.action}).subscribe(
      ans => {
        this.coreService.setLogin(ans.playerId, true);
        this.router.navigate(['/']);
      }
    );
  }

}
