import { Injectable } from '@angular/core';
import { PlayTable, TriviaSubject } from '../models/play-table';
import { CoreService } from '../core.service';
import { FirebaseService } from "../firebase.service";
import { TriviaTableDTO, GameParticipationDTO } from "@brainerz/common/lib";
import { ReplaySubject } from "rxjs/ReplaySubject";
import "rxjs/add/operator/shareReplay";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/do";
import "rxjs/add/operator/reduce";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/take";
import "rxjs/add/observable/of";
import "rxjs/add/observable/from";
import "rxjs/add/observable/forkJoin";
import "rxjs/add/observable/forkJoin";
import { Observable } from "rxjs/Observable";
import { TriviaGameParticipation } from "../models/game/trivia-game/trivia-game-participation";

@Injectable()
export class TablesService {

  tables$: Observable<PlayTable[]>;

  constructor(coreService: CoreService, private fireService: FirebaseService){
    this.tables$ = this.fireService.db.tables.snapshotChanges().do(d =>
      console.log("table update")).mergeMap(changes =>
      Observable.from(changes).mergeMap(change =>
        Observable.of(<TriviaTableDTO>change.payload.doc.data()).mergeMap(tableDTO =>
                 Observable.forkJoin(
                  this.fireService.collection<GameParticipationDTO>(change.payload.doc.ref.path + "/" + 'participations').
                  valueChanges().map(participations => {
                    const table = PlayTable.fromDTO(tableDTO, change.payload.doc.id);
                    table.participations = participations.map(p =>
                      TriviaGameParticipation.fromDTO(p)); return table; }).take(1).
                  catch(e => {console.error("errorrrr", e); return Observable.of(e); }))))).
              reduce((acc, val) => acc.concat(val), []).
              shareReplay();

              this.fireService.db.tables.valueChanges().subscribe(a => console.log("update to table update to table upp"));
  }
        //   let table =
        //   .

        //   PlayTable.fromDTO(table,
        //                     t.payload.doc.id,
        //                   t.payload.doc.data().))).
        // shareReplay();
  }

  const TMP_TEST_currDate = new Date(); // used only for testing fake data

  // tables = new Map<string, PlayTable>([
  //     ["PZ7DWkgFF7jgJKuMsN2E", { id: "PZ7DWkgFF7jgJKuMsN2E",
  //     subject: TriviaSubject.subjects.history,
  //      openTime: new Date(this.TMP_TEST_currDate.valueOf() - 400000),
  //      endTime:  new Date(this.TMP_TEST_currDate.valueOf() + 400000),
  //      entranceFee: 50,
  //      participations: null
  //     }], ["2",
  //     {
  //       id: "2",
  //       subject: TriviaSubject.subjects.sports,
  //       openTime: new Date(this.TMP_TEST_currDate.valueOf() - 400000),
  //       endTime:  new Date(this.TMP_TEST_currDate.valueOf() + 450000),
  //       entranceFee: 10,
  //       participations: null
  //      }],[ "3",
  //      {id:"3",
  //       subject: TriviaSubject.subjects.science,
  //       openTime: new Date(this.TMP_TEST_currDate.valueOf() - 400000),
  //       endTime:  new Date(this.TMP_TEST_currDate.valueOf() + 5500000),
  //       entranceFee: 5,
  //       participations: null
  //      }],[ "4",
  //      {
  //        id: "4",
  //        subject: TriviaSubject.subjects.movies,
  //       openTime: new Date(this.TMP_TEST_currDate.valueOf() - 400000),
  //       endTime:  new Date(this.TMP_TEST_currDate.valueOf() + 44400000),
  //       entranceFee: 50,
  //       participations: null
  //      }]
  //   ]);

