import { Component, OnInit, ViewChild } from '@angular/core';
import { TablesService } from './tables.service';
import { PlayTable } from '../models/play-table';
import { FirebaseService } from '../firebase.service';
import { Router } from '@angular/router';
import { joinTableAction } from '@brainerz/common/lib/api';
import { CoreService } from '../core.service';

@Component({
  selector: 'brz-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {
  @ViewChild("navbar") navbar;
  tableListHeight = 0;
  tables: {top: PlayTable, bottom: PlayTable}[] = [];
  isAppBusy = false;

  constructor(public lobbyService: TablesService, private fireService: FirebaseService,
    private coreService: CoreService, private router: Router) {

   }

  ngOnInit() {
    this.coreService.finishedLoadingDataFromDisk$.filter(ev => ev === true). subscribe( e => {
      if (!this.coreService.player){
        this.router.navigate(["./signup"]);
      }
    });
    this.tableListHeight = document.getElementById("app-wrapper").clientHeight -
                          document.getElementById("lobby-navbar").clientHeight -
                          document.getElementById("lobby-tab-bar").clientHeight - 5;
    console.log("subsssss", this.lobbyService.tables$);
    this.lobbyService.tables$.subscribe(tables => {
      console.log("subs", tables);
      for (let i = 0 ; i < tables.length; i += 2){
        this.tables[i / 2] = {top: tables[i], bottom: tables[i + 1]};
      }
    });

  }

  joinTable(tableId: string){
    if (!this.coreService.isAppBusy){

      this.fireService.request(joinTableAction.action.send({tableId: tableId})).subscribe(r => {
        console.log("result: ", r);
        this.router.navigate(['/game', r.gameId, 'play'/*should be table-id and game-id will be taken from DB*/]);
      }, e => console.error("error", e));
    }
  }
}
