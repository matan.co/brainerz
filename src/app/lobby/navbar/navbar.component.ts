import { Component, OnInit, Output } from '@angular/core';
import { CoreService } from '../../core.service';
import { images } from '../../utils/type-safe-image-paths';

@Component({
  selector: 'brz-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  images = images;
  constructor(public coreService: CoreService) {
   }

  ngOnInit() {
  }

}
