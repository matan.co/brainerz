import { shuffleArray } from '../../../utils/utils';
import { TriviaAnswer } from "@brainerz/common/lib";

// export class TriviaQuestionData {
//     id: number;
//     text: string;
//     wrongAns: Answer[];
//     rightAns: Answer;
// }
export class TriviaQuestion {
    static tempIdSequence = 0; // until there is backend
    static readonly TimeOutAnswer = -2;
    static readonly AskedHelpAnswer = -3;

    id: string;
    answers: Answer[] = [];
    rightAnsId: number;

    constructor(public text: string, rightAnswer: string, wrongAnswers: string[]) {
        // this.id = TriviaQuestion.tempIdSequence++;
        const indexArr = shuffleArray([0, 1, 2, 3]);
        this.rightAnsId = indexArr.pop();
        this.answers[this.rightAnsId] = new Answer(this.rightAnsId, rightAnswer); // SECURITY!!! the correct answer will be always index 0;
        wrongAnswers.forEach( ans => {
            const nextQustionId = indexArr.pop();
            this.answers[nextQustionId] = (new Answer(nextQustionId, ans));
        });
    }

    // /// returns the strings shuffled and a boolArray represtnting the index of the corr
    // shuffledAnswers(): {correctAnswerIndex: number, answers: string[]} {
    //     // the correct answer is represnted by index 0
    //     // using this way makes the length of the wrong answers variable
    //     const CORRECT_ANS_INDEX = 0;
    //     const indexArray = shuffleArray([CORRECT_ANS_INDEX].concat(this.wrongAns.map((v, ind) => ind + 1)));
    //     let correctAnswerIndex;
    //     const shuffledAnswers = indexArray.map( (answerIndex, IndexOfIndexArray) => {
    //         if (answerIndex === CORRECT_ANS_INDEX) {
    //             correctAnswerIndex = IndexOfIndexArray;
    //             return this.rightAns;
    //         } else {
    //             return this.wrongAns[answerIndex - 1]; // This makes sure the false answers are also shuffled
    //         }
    //     });
    //     return { correctAnswerIndex , answers: shuffledAnswers };
    // }
}

export class Answer implements TriviaAnswer{
    constructor (public id: number, public text: string) {}
}

// export class SolvedQuestion {

//     get question() { return this.fullQuestionData.text; }
//     get id() {return this.fullQuestionData.id; }
//     get answers() {return this.fullQuestionData.answers; }
//     get solution(): SolveStatus {
//         if (this.selectedAnswerId === TriviaQuestion.TimeOutAnswer) {
//             return SolveStatus.Unanswered;
//         } else {
//             return this.selectedAnswerId === this.fullQuestionData.rightAnsId ? SolveStatus.Correct : SolveStatus.Incorrect;
//         }
//     }

//     getSolutionClassName(): 'u-incorrect-bg' | 'u-correct-bg' | '' {
//         switch (this.solution) {
//             case SolveStatus.Correct:
//                 return 'u-correct-bg';
//             case SolveStatus.Incorrect:
//                 return 'u-incorrect-bg';
//             default: return '';
//         }
//     }

//     constructor(public fullQuestionData: TriviaQuestion , public selectedAnswerId: number,  public solveTime: number) {
//     }
// }


export enum SolveStatus {
    Correct,
    Incorrect,
    Unanswered
}
