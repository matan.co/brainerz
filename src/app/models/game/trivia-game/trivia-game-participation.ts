import { Player } from '../../player';
import { TriviaQuestion, SolveStatus } from './base-question';
import {GameParticipationDTO, AnsweredQuestion } from "@brainerz/common";


export class TriviaGameParticipation {
    player: string;
    answers: AnsweredQuestion[] = [];
    correctCount: number;

    sumGame() {
        this.correctCount = this.answers.filter(a => a.solution === SolveStatus.Correct).length;
    }

    // tslint:disable-next-line:member-ordering
    static fromDTO(dto: GameParticipationDTO): TriviaGameParticipation{
        const participation = new TriviaGameParticipation();
        participation.player = dto.player;
        // participation.answers = dto.answers;
        participation.correctCount = dto.correctCount;
        return participation;
    }
}
