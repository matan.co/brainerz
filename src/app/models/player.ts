import { PhoneNumberType, PlayerIdType } from "./common/types";
import { PlayerDTO } from '@brainerz/common';
import { images } from "../utils/type-safe-image-paths";

export class Player {

    id: PlayerIdType;
    phoneNumber: PhoneNumberType;
    facebookId: string;
    displayName: string;
    pointsBalance: number;
    avatar: string;

    constructor(){
    }

    static fromPlayerDTO(dto: PlayerDTO){
        const player = new Player();
        player.displayName = dto.nickname;
        player.avatar = images.avatars + dto.avatar + ".png";
        player.pointsBalance = dto.pointsBalance;
        return player;
    }
}
