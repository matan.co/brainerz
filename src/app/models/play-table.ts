import { TriviaGameParticipation } from './game/trivia-game/trivia-game-participation';
import { TriviaTableDTO, GameParticipationDTO } from "@brainerz/common/lib";
import { printCountDown } from "../utils/utils";

export class PlayTable {
    id: string;
    openTime: Date;
    closeTime: Date;
    entranceFee: number;
    subject: TriviaSubject;
    participations: TriviaGameParticipation[] = [];
    numOfQuestions: number;

    static fromDTO(dto: TriviaTableDTO, id: string ){
        const table = new PlayTable();
        table.id = id;
        table.openTime = dto.openTime;
        table.subject = TriviaSubject.subjects[dto.subject];
        table.entranceFee = dto.entranceFee;
        table.closeTime = dto.closeTime;
        table.numOfQuestions = dto.numOfQuestions;
        return table;
    }

    totalMoney() {
        return (this.participations.length * this.entranceFee).toLocaleString();
    }

    timeToClose(): string {
        const timespan = new Date(this.closeTime.valueOf() - new Date().valueOf());
        return printCountDown(timespan);
    }
}

export class TriviaSubject{

    title: string;
    constructor(public name: string){
        this.title = this.name.charAt(0).toUpperCase() + this.name.slice(1);
    }

    // tslint:disable-next-line:member-ordering
    public static readonly subjects = {
        "sports": new TriviaSubject("sports"),
        "history": new TriviaSubject("history"),
        "movies": new TriviaSubject("movies"),
        "science" : new TriviaSubject("science")
    };
}