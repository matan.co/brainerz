import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreService } from './core.service';
import { TablesService } from './lobby/tables.service';
import { AngularFireModule,  } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FirebaseService } from './firebase.service';
import { MyHttpInterceptor } from './utils/httpInterceptor';
import { MiscModule } from "./misc/misc.module";
import { AsyncLocalStorageModule } from 'angular-async-local-storage';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AsyncLocalStorageModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,

    MiscModule
  ],
  providers: [CoreService, TablesService, FirebaseService, { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
