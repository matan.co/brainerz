import { Component } from '@angular/core';
import { CoreService } from './core.service';
import {AnimationEvent, trigger, animate, style, group, animateChild, query, stagger, transition} from '@angular/animations';

@Component({
  selector: 'brz-root',
  template:
  `<div id="app-wrapper" [@routerTransition]="getState(o)"
    (@routerTransition.start)="animStart($event)">
<div class="shape"></div>
<router-outlet #o="outlet"></router-outlet></div>`,
  styleUrls: ['./app.component.scss'],
  animations: [trigger('routerTransition', [
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width:'100%' })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ], { optional: true }),
    ])
  ])
])]
})
export class AppComponent {

  constructor(coreService: CoreService){
    coreService.getLogin();
  }

  ngOnInit(){
    
    let html = '';
    for (let i = 1; i <= 20; i++) {
      html += '<div class="shape-container--' + i + ' shape-animation"><div class="random-shape"></div></div>';
    }

    document.querySelector('.shape').innerHTML += html;
    const $allShapes = document.querySelectorAll("[class*='shape-container-']");
  }

  getState(outlet){
        return outlet.activatedRouteData.state || "firstPage";
  }

  animStart(event: AnimationEvent){
    console.log("event", event);
    if (event.fromState = "lobby"){
      if (event.toState = "game") {
      //const audio: HTMLAudioElement = <HTMLAudioElement>document.getElementById("audio-enter-table");
      //audio && audio.play();
    }
    }
  }
}
