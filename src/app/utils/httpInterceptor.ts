import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CoreService } from '../core.service';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

    constructor(private coreService: CoreService){
        console.log("http interceptor");
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.coreService.player){ // Authed request
            req = req.clone({setHeaders: {'x-player': this.coreService.player.id }});
        } else {
            console.log("sending unauthed request!");
        }
        return next.handle(req);
    }
}
