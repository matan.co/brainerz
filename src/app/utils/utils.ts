// Makes a Date object representing time span as a countdown string
// "03:22:05" (HH:MM:SS)
export function printCountDown(timeSpan: Date): string {
    const result =
    ('0' + timeSpan.getHours()).slice(-2) + ':'
    + ('0' + timeSpan.getMinutes()).slice(-2) + ':'
    + ('0' + timeSpan.getSeconds()).slice(-2);
    return result;
}

export function shuffleArray<T>(a: Array<T>): Array<T> {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}
