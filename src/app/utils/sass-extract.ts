import * as extract from "sass-extract";
declare var require;
const style = require('sass-extract-loader?{"plugins": ["compact"]}!../../styles/_subjects.scss');

export interface SubjectStyles {
    gradStart: {hex: string};
    gradEnd: {hex: string};
    color3?: {hex: string};
}
export function extractSass(subject: string): SubjectStyles{

    console.log("styleee ", style);
    if (style && style.global){
        const styleObjects = <object>style.global["$" + subject];
        console.log('objectifie', styleObjects);
        return <SubjectStyles>styleObjects;
    }
}
