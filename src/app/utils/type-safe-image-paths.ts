
//           TYPE SAFE IMAGE PATHS
//      #################################
//
// in order to create single source of truth for image paths
// this utility build a set of objects that maps the paths into objects
//
// USAGE: by calling  createImageConsts function (see documentation above it)


// this consts represent the assets directory formation.
// the toString() overrides allow me to concat the children to string to create paths
const assets = "assets/";
const imagesBase = assets + "images/";
 const imageCommon = imagesBase + "common/";
const imageGame = imagesBase + "game/";
const imagesubjects = imagesBase + "subjects/";
const lobby = imagesBase + "lobby/";
const avatars = imagesBase + "avatars/";

export const images = { // use only on most basic cases when need only common
    toString: () => imagesBase,
    common: {
        toString: () => imageCommon,
        coin: imageCommon + "coin.png",
        spinner: imageCommon + "spinner.svg",
        backArrow: imageCommon + "back.svg"
    },
    subjects: {
        toString: () => imagesubjects,
        symbols:  imagesubjects + "symbols/",
        miniSymbols:  imagesubjects + "mini-symbols/"
    },
    game: {
        toString: () => imageGame,
        medal: imageGame + "medal.svg"
    },
    lobby: {
        toString: () => lobby
    },
    avatars: {
        toString: () => avatars
    },
    background: {
        toString: () => imagesBase + "background/"
    }
};

// createImageConsts
// ##################
// call it in you controller and save the result to a member there,
//
// PARAMS:
// addPaths - a function (callback) that receives
//              the `images` object defined here in this page.
//              so it can be used to add more image paths for that controller needs.
//              - it gives you the base paths just concat further to create the path.
//
// Returns: an object that has all the built in paths of this `images` object
//          plus the extra "path" objects returned from calling addPaths() callback.
export function createImageConsts<T>(addPaths: (imgConsts: typeof images) => T){
    return Object.assign({}, images, addPaths(Object.freeze(images)));
}

// export namespace images {
//     const toString = () => imagesBase;

//     export const common = {
//         toString: () => imageCommon,
//         coin: imageCommon + "coin.png"
//     };
//     export const subjects = {
//         toString: () => imageSubjects,
//         symbols:  imageSubjects + "symbols/",
//         miniSymbols:  imageSubjects + "mini-symbols/"
//     };
// }
