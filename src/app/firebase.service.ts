import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpApiFunction } from "@brainerz/common/lib/api";
import {OnGoingGameDTO, GameParticipationDTO, TriviaTableDTO} from "@brainerz/common";
import {environment} from "../environments/environment";
@Injectable()
export class FirebaseService {


   readonly BaseUrl = environment.serverUrl;


  constructor(private http: HttpClient, public firestore: AngularFirestore) {
  }

  db = {
    onGoingGames: this.firestore.collection<OnGoingGameDTO>("onGoingGames"),
    tables: this.firestore.collection<TriviaTableDTO>("tables")
  };

  collection<T>(path: string): AngularFirestoreCollection<T>{
    return this.firestore.collection<T>(path);
  }

  // USE HttpApiFunction.send();
  request<T, R>(input: {data: T, action: HttpApiFunction<T, R>}){
      return this.http.post<R>(this.BaseUrl + input.action.name, input.data);
  }

}
