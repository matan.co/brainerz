import { Injectable } from '@angular/core';
import { Player } from './models/player';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { PlayerDTO } from '@brainerz/common';
import { Location } from '@angular/common';
import { AsyncLocalStorage } from 'angular-async-local-storage';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

declare var FB;
@Injectable()
export class CoreService {

  player: Player;
  playerRef: AngularFirestoreDocument<PlayerDTO>;
  finishedLoadingDataFromDisk$ = new BehaviorSubject(false);

  isAppBusy = false;

  constructor(public db: AngularFirestore, public location: Location, protected localStorage: AsyncLocalStorage) {

    // That isn't good that this is a-sync because it will cause redirect to signup
    localStorage.getItem("playerId").subscribe( id => { if (id) {this.setLogin(id); this.finishedLoadingDataFromDisk$.next(true); }});
   }

  getLogin(){
    setTimeout( () =>
    FB.getLoginStatus(response => {
      console.log("facebook login response: ", response);
    }), 500);
  }

  setLogin(playerId: string, saveLocal?: boolean){
    console.log("setting login");
      this.playerRef = this.db.doc<PlayerDTO>('/players/' + playerId);
      const subs = this.playerRef.valueChanges().subscribe( p => {
        console.log("setting at local sto");
        if (saveLocal){
          this.localStorage.setItem("playerId", playerId).subscribe(); // Cold subscritipn?
        }
        this.player = Player.fromPlayerDTO(p);
        this.player.id = this.playerRef.ref.id;
        subs.unsubscribe();
    });
  }

}
