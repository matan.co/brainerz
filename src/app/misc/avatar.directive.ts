
import { Directive, OnChanges, Input, ElementRef, HostListener } from "@angular/core";

@Directive({
    selector: '[brzAvatar]',
  })
  export class AvatarDirective implements OnChanges{
      @Input() brzAvatar: string;

      element: HTMLImageElement;

      constructor(el: ElementRef){
        this.element = el.nativeElement;
      }

      ngOnChanges(){
        this.element.src = `/assets/images/avatars/${this.brzAvatar}.png`;
        this.element.dataset.avatar = this.brzAvatar;
      }

      @HostListener('click') onClick(){
          console.log("click!");
      }

  }
