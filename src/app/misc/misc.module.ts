import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { MenuBtnComponent } from './menu-btn.component';
import { AvatarDirective } from "./avatar.directive";

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [MenuBtnComponent, AvatarDirective],
  exports: [MenuBtnComponent, AvatarDirective]
})
export class MiscModule { }
