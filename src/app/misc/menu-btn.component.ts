import { Component, OnInit, Output, EventEmitter } from '@angular/core';

const template = `<div class="menu-btn" (click)="click()" [ngClass]="{'change': opened}">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
</div>`;

const css =
`
 .menu-btn{
    display: inline-block;
    cursor: pointer;
}
.menu-btn>div {
    width: 35px;
    height: 5px;
    background-color: #9b9b9b;
    border-radius:15px;
    margin: 6px 0;
    transition: 0.4s;
}
.change .bar1 {
    -webkit-transform: rotate(-45deg) translate(-9px, 6px) ;
    transform: rotate(-45deg) translate(-9px, 6px) ;
}

.change .bar2 {opacity: 0;}

.change .bar3 {
    -webkit-transform: rotate(45deg) translate(-8px, -8px) ;
    transform: rotate(45deg) translate(-8px, -8px) ;
}`;

@Component({
  selector: 'brz-menu-btn',
  template: template,
  styles: [css]
})
export class MenuBtnComponent {
    opened = false;

  @Output() clickEvent = new EventEmitter();

  click(){
      this.opened = !this.opened;
      this.clickEvent.emit(this.opened);
  }
}
