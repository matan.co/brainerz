import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'signin', loadChildren: './misc/misc.module#MiscModule'},
  {path: '', redirectTo: '/lobby', pathMatch: 'full'},
  {path: 'lobby', loadChildren: './lobby/lobby.module#LobbyModule', data: {state: "lobby"}},
  {path: 'game', loadChildren: './game/game.module#GameModule',  data: {state: "game"}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { })],
  exports: [RouterModule]
})
export class AppRoutingModule { }