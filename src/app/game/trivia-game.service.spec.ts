import { TestBed, inject } from '@angular/core/testing';

import { TriviaGameService } from './trivia-game.service';

describe('TriviaGameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TriviaGameService]
    });
  });

  it('should be created', inject([TriviaGameService], (service: TriviaGameService) => {
    expect(service).toBeTruthy();
  }));
});
