import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { TriviaGameParticipation } from '../../../models/game/trivia-game/trivia-game-participation';
import { SolveStatus } from '../../../models/game/trivia-game/base-question';
import { Router } from '@angular/router';
import { createImageConsts } from '../../../utils/type-safe-image-paths';
import { TriviaGameService } from '../../trivia-game.service';
import { OnGoingGameDTO } from '@brainerz/common';

@Component({
  selector: 'brz-game-end',
  templateUrl: './game-end.component.html',
  styleUrls: ['./game-end.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GameEndComponent implements OnInit {
  @Input() game: OnGoingGameDTO;
  constructor( private router: Router, public gameService: TriviaGameService) { }
  images = this.gameService.assets;

  ngOnInit() {
    console.log("game table", this.game.table);
  }

  public correctAnswers(){
    console.log("corr ansss", this.game.answers);
    return this.game.answers.reduce((p, n) => p += n.solution === SolveStatus.Correct ? 1 : 0, 0);
  }
}
