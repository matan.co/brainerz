import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { createImageConsts } from '../../../utils/type-safe-image-paths';
import { TriviaGameService } from '../../trivia-game.service';
import { CoreService } from '../../../core.service';

@Component({
  selector: 'brz-game-getstarted',
  templateUrl: "./get-started.component.html",
  styleUrls: ["./get-started.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class GetStartedComponent implements OnInit {
  images = this.gameService.assets;

  constructor(public gameService: TriviaGameService, public coreService: CoreService) {

   }
  @Output() clickPlay = new EventEmitter();


  ngOnInit() {
  }

}
