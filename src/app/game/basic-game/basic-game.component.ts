import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { TriviaQuestion } from '../../models/game/trivia-game/base-question';
import { TriviaGameService } from '../trivia-game.service';
import { TablesService } from '../../lobby/tables.service';
import { ActivatedRoute } from '@angular/router';
import { PlayTable } from '../../models/play-table';
import { FirebaseService } from '../../firebase.service';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { createImageConsts } from '../../utils/type-safe-image-paths';
import { answerQuestionAction } from '@brainerz/common/lib/http-api/actions';
import { OnGoingGameDTO } from '@brainerz/common';
import { Observable } from 'rxjs/Observable';
import { CoreService } from "../../core.service";

@Component({
  selector: 'brz-basic-game',
  templateUrl: './basic-game.component.html',
  styleUrls: ['./basic-game.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BasicGameComponent implements OnInit {
  table: PlayTable;
  isHelp = false;
  images = this.gameService.assets;

  constructor(public gameService: TriviaGameService,
    private coreService: CoreService,
    private fireBaseService: FirebaseService,
    private route: ActivatedRoute,
    public tableService: TablesService) {
    }

    dbGame: Observable<OnGoingGameDTO>;

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.gameService.initGame(p.gameId);
    });
  }

  startGame() {
    this.gameService.startGame();
  }

  async answered(answerId: number) {
    const rightAnsFromSrv = await this.gameService.answerQuestion(answerId);

    //////////////////////////////////////////////////////////////////////////////////////////// provide color feedback
    const userAnswer = document.getElementsByClassName('ans' + answerId).item(0);
    const rightAnswer = document.getElementsByClassName('ans' + rightAnsFromSrv.rightAnswerId).item(0);
    console.log("answered ", userAnswer);
    if (userAnswer && userAnswer !== rightAnswer) {
      userAnswer.classList.add('u-incorrect-bg');
    }
    rightAnswer.classList.add('u-correct-bg');

    // const t = setTimeout(() => {
    //   // get next question
    //   this.initQuestion();
    //   clearTimeout(t);
    // }, 1000);
  }

  getHelp() {
    this.gameService.stopTimer();


    this.isHelp = true;
  }
}
