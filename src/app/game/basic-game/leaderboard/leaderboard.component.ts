import { Component, OnInit } from '@angular/core';
import { TriviaGameService } from '../../trivia-game.service';
import { FirebaseService } from '../../../firebase.service';
import { GameParticipationDTO, TriviaTableDTO, getRanksWorth } from '@brainerz/common';
import { ActivatedRoute } from '@angular/router';
import { TriviaGameParticipation } from '../../../models/game/trivia-game/trivia-game-participation';
import { AngularFirestoreDocument } from 'angularfire2/firestore';
import { CoreService } from '../../../core.service';
import { images } from '../../../utils/type-safe-image-paths';
import { TablesService } from "../../../lobby/tables.service";
import { PlayTable } from "../../../models/play-table";


@Component({
  selector: 'brz-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
  tableId: string;
  table: PlayTable;
  ranks: Array<number> = [];
  ranksRange: Array<number>;
  playerParticipation: TriviaGameParticipation;
  images = images;
  ranksWorth: Array<number>;
  constructor(public fireService: FirebaseService, public coreService: CoreService,
              public gameService: TriviaGameService, // Shouldn't be here - subject should be taken from table
              private route: ActivatedRoute,
              private tableService: TablesService) {
      route.params.first().subscribe(r => {
        this.tableId = r["tableId"];
        this.tableService.tables$.subscribe(tables => { this.setTable(tables.find(t => t.id === this.tableId)); });
      });
   }

  ngOnInit() {


  }

  setTable(table: PlayTable){
    console.log("setting table ", table);
    this.table = table;
    this.ranks.length = table.numOfQuestions + 1; // including zero
    this.ranks = this.ranks.fill(0);
    this.ranksRange = Array.from(this.ranks, (x, i) => i).reverse().slice(0, 6);
    this.table.participations.forEach(p => {
      this.ranks[p.correctCount]++;
    });

    this.playerParticipation = this.table.participations.find(part => part.player === this.coreService.player.id);

    this.ranksWorth = getRanksWorth(this.table.participations, this.table).map( r => Math.round(r));
    console.log("aaa", this.ranksWorth);
  }

}
