import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicGameComponent } from './basic-game/basic-game.component';
import { SimpleQuestionComponent } from './simple-question/simple-question.component';
import { RouterModule } from '@angular/router';
import { TriviaGameService } from './trivia-game.service';
import { GameEndComponent } from './basic-game/screens/game-end.component';
import { GetHelpComponent } from "./get-help/get-help.component";
import { GetStartedComponent } from './basic-game/screens/get-started.component';
import { LeaderboardComponent } from './basic-game/leaderboard/leaderboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: ':gameId/play', component: BasicGameComponent},
      {path: ':tableId/leaderboard', component: LeaderboardComponent}
    ])
  ],
  declarations: [BasicGameComponent, SimpleQuestionComponent, GameEndComponent,
                  GetHelpComponent, GetStartedComponent, LeaderboardComponent],
  providers: [TriviaGameService]
})
export class GameModule { }
