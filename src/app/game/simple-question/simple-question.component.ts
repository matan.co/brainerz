import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges, OnChanges, ViewEncapsulation } from '@angular/core';
import { TriviaQuestion } from '../../models/game/trivia-game/base-question';
import { trigger, state, style, transition, animate, AnimationEvent } from '@angular/animations';
import { TriviaGameService } from "../trivia-game.service";
import { TriviaQuestionDTO } from '@brainerz/common';
import { extractSass } from '../../utils/sass-extract';

@Component({
  selector: 'brz-simple-question',
  templateUrl: './simple-question.component.html',
  styleUrls: ['./simple-question.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('question', [
      state('in', style({
        transform: 'translateX(0)'
      })),
      state('not-visible', style({
          visibility: 'hidden'
      }))
    , transition('* => in', [style({
        transform: 'translateX(150%)'
      }), animate('200ms ease-out')])
    ]),
  ]
})
export class SimpleQuestionComponent implements OnInit, OnChanges {
  @Input() question: TriviaQuestionDTO;
  secondsLeftToAnswer: number;
  readonly TimeToAnswerInMS = 7000;
  currentlyAnimating = 0;
  ctx: CanvasRenderingContext2D;

  @Output() answered = new EventEmitter<number>();
  constructor(private gameService: TriviaGameService) {

   }

  ngOnInit() {
    this.ctx = (<any>document.getElementById("timer-canvas")).getContext('2d');
    this.configCanvas();
    this.gameService.addTimerCallback(this.timerCallBack.bind(this));
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.hasOwnProperty('question')) {
      this.ctx && this.ctx.clearRect(0, 0, 55, 55);
      this.currentlyAnimating = 0;
      this.secondsLeftToAnswer = this.TimeToAnswerInMS / 1000;
    }
  }

  animate(questionIndex) {
    if (questionIndex === this.currentlyAnimating) {
        return 'in';
    } else if (questionIndex > this.currentlyAnimating) {
        return 'not-visible';
    } else {
        return 'was-animated';
    }
  }

  animationDone(questionIndex, event: AnimationEvent) {
    if (event.toState === 'in' && questionIndex === this.currentlyAnimating) {
      this.currentlyAnimating++;
      if (questionIndex === this.question.answers.length - 1) {
        this.startCounting();
      }
    }
  }

  answer(answerId: number)  { // Or TriviaQuestion.TimeoutAnswer
    if (answerId !== TriviaQuestion.TimeOutAnswer) {
      const audio: HTMLAudioElement = <HTMLAudioElement>document.getElementById("audio-tap-answer");
      //audio.play();
      setTimeout(function() {
        audio.pause();
        audio.load();
      }, 1000);
    }
    this.answered.emit(answerId);
  }

  timerCallBack(startTime: Date, timeSpan) {
    this.ctx.clearRect(0, 0, 55, 55);
    this.ctx.beginPath();
    let partOfTimeLimit = (this.TimeToAnswerInMS - timeSpan) / this.TimeToAnswerInMS;
    this.secondsLeftToAnswer = Math.ceil((this.TimeToAnswerInMS - timeSpan) / 1000);
    if (this.secondsLeftToAnswer === 0) {
        this.timeIsUp();
        partOfTimeLimit = 0; // To prevent negative values
    }
    this.ctx.arc(26, 26, 24, 0, partOfTimeLimit * 2 * Math.PI);
    this.ctx.stroke();
    this.ctx.fillText(this.secondsLeftToAnswer.toString(), 20, 33);
  }

  timeIsUp() {
      this.answer(TriviaQuestion.TimeOutAnswer);
  }

  startCounting() {
      // Set Timer
      this.gameService.startTimer();
  }

  configCanvas() {
    const styles = extractSass(this.gameService.subject);
    this.ctx.font = "20px sans-serif";
    this.ctx.fillStyle = styles.color3.hex;
    this.ctx.lineWidth = 4;
    this.ctx.strokeStyle = styles.gradEnd.hex;
  }

}
