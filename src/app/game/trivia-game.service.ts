import { Injectable } from '@angular/core';
import {  TriviaGameParticipation } from '../models/game/trivia-game/trivia-game-participation';
import { TriviaQuestion } from '../models/game/trivia-game/base-question';
import { createImageConsts } from '../utils/type-safe-image-paths';
import { OnGoingGameDTO, GameStatus, startGameAction, TriviaTableDTO } from '@brainerz/common';
import { Observable } from 'rxjs/Observable';
import { FirebaseService } from '../firebase.service';
import { AngularFirestoreDocument } from 'angularfire2/firestore';
import { answerQuestionAction } from '@brainerz/common/lib/api';
import "rxjs/add/operator/do";
import "rxjs/add/operator/first";
import { extractSass } from '../utils/sass-extract';
import { TablesService } from "../lobby/tables.service";
import { PlayTable } from "../models/play-table";

@Injectable()
export class TriviaGameService {

  constructor( private fireService: FirebaseService, private tablesService: TablesService) { }

  subject = "science";
  dbGameDoc: AngularFirestoreDocument<OnGoingGameDTO>;
  table: PlayTable;


  assets = createImageConsts(imgConsts => ({
    symbol: imgConsts.subjects.symbols + this.subject + ".svg",
    miniSymbol: imgConsts.subjects.miniSymbols + this.subject + ".svg"
  }));

  currQuestionShowTime = new Date();
  timer;
  timerCallbacks: Function[] = [];
  timePassedInMs: number;
  game: OnGoingGameDTO;

  initGame(gameId: string) {
    this.dbGameDoc = this.fireService.db.onGoingGames.doc(gameId);
    const game$ = this.dbGameDoc.valueChanges().subscribe(v => {
      this.tablesService.tables$.map( t => t.find(tt => tt.id === v.table.id)).subscribe( table =>
        this.table = table
      );
      // this.fireService.db.tables.doc<TriviaTableDTO>(v.table.id).snapshotChanges().subscribe(table => {
      //   this.table.subject = this.tablesService.tables[table.payload.data().subject];
      //   this.table = table;
      // });

      console.assert(!(this.game && this.game.currQuestion && v.currQuestion &&
                      this.game.currQuestion.id === v.currQuestion.id), "an update happend that wasn't a question change" +
                    "this will cause a question redraw and is probably a bug on the server side");
      this.game = v;
    });
  }

  startGame(){
    this.fireService.request(startGameAction.action.send(
                              {gameId: this.dbGameDoc.ref.id})).subscribe(); // get first question - empty request
  }

  async answerQuestion(answerId: number): Promise<{rightAnswerId: number}>{
    console.log("answer q");
    if (this.game.status !== GameStatus.InPlayActive) {
      return;
    } else {
      this.stopTimer();
      const result = await this.fireService.request(
                            answerQuestionAction.action.send({
                              gameId: this.dbGameDoc.ref.id,
                              questionId: this.game.currQuestion.id,
                              answerId: answerId})).toPromise();
      return {rightAnswerId: result.rightAnswer};
    }
  }

  endGame() {
    // Calc
  }

  isNew(){
      return (!this.game) || this.game.status === GameStatus.New;
  }
  isInPlay(): boolean {
      return this.game && [GameStatus.InPlayActive, GameStatus.InPlayWaiting].indexOf(this.game.status) > -1; // includes
  }
  hasEnded(): boolean {
      return this.game && this.game.status === GameStatus.Ended;
  }

  addTimerCallback(cb: (startedTime: Date, timeSpan: number) => any) {
    this.timerCallbacks.push(cb);
  }

  startTimer() {
    console.log("starting timer");
    this.timer = setInterval( startedTime => {
      this.timePassedInMs = new Date().valueOf() - startedTime.valueOf();
      this.timerCallbacks.forEach(cb => cb(startedTime, this.timePassedInMs));
    }, 50, new Date());
  }

  stopTimer() {
    clearInterval(this.timer);
  }
}
